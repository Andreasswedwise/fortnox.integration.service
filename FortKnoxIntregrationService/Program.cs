﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Swedwise.Integration.Fortnox;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using Swedwise.Integration.Fortnox.Properties;
using System.Collections.Specialized;
using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using DataCaptureFileBasedIntegrationLib;
using ReadSoft.Online.Samples;


namespace FortKnoxIntregrationService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new FortnoxService()
            };
            ServiceBase.Run(ServicesToRun);

            
          
    }
    }
}
