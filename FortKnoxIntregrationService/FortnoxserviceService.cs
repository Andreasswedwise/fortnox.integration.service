﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Swedwise.Integration.Fortnox;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using Swedwise.Integration.Fortnox.Properties;
using System.Collections.Specialized;
using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using DataCaptureFileBasedIntegrationLib;
using ReadSoft.Online.Samples;
using System.Threading;

namespace FortKnoxIntregrationService
{
    public partial class FortnoxService : ServiceBase
    {
        private Thread thread;
        public FortnoxService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            thread = new Thread(FortnoxLoop);
            thread.IsBackground = true;
            thread.Start();

        }

        protected override void OnStop()
        {
            thread.Abort();
        }

        public void FortnoxLoop()
        {
            try
            {
                Debugger.Launch();
                Dictionary<string, Fortnox> connections = new Dictionary<string, Fortnox>();

                var connectionDoc = XDocument.Load(Settings.Default.FortnoxConfigurationPath);

                foreach (var connection in connectionDoc.XPathSelectElements("/Connections/Connection"))
                {
                    var f = new Fortnox(Settings.Default.FortnoxApi, connection.Element("Database").Value, connection.Element("Token").Value, connection.Element("Secret").Value, connection.Element("DefaultPurchaseAccount").Value);
                    f.Simulate = Settings.Default.SimulateFortnox;
                    f.SetAsActive();
                    f.LoadSuppliers();
                    f.LoadAccounts();
                    f.LoadCostCenters();
                    connections.Add(connection.Element("Id").Value, f);
                }


                EmailClient emailClient = new EmailClient();
                emailClient.UserName = Settings.Default.SmtpUserName;
                emailClient.Password = Settings.Default.SmtpPassword;
                emailClient.UseDefaultCredentials = Settings.Default.SmtpUseDefaultCredentials;
                emailClient.Timeout = Settings.Default.SmtpTimeout;
                emailClient.Port = Settings.Default.SmtpPort;
                emailClient.Host = Settings.Default.SmtpServer;
                emailClient.IgnoreMissingAttachments = true;
                emailClient.UseSSL = Settings.Default.SmtpUseSSL;

                emailClient.Init();
                string emailSender = Settings.Default.EmailSender;
                string emailReceiver = Settings.Default.ArchiveReceiver;
                string emailErrorReceiver = Settings.Default.EmailErrorReceiver;
                emailClient.Simulate = Settings.Default.SimaulateEmail;


                List<KeyValuePair<string, string>> errorFiles = new List<KeyValuePair<string, string>>();

                ClientConfiguration clientConfig = null;
                //bool errorOccured = false;
                while (true) // "fun fun fun til her daddy takes the endless-loop away"
                {
                    System.Threading.Thread.Sleep(15000);

                    try
                    {
                        // Authenticate 

                        if (clientConfig == null)
                            clientConfig = DataCaptureFileBasedIntegrationLib.ProgramReadsoft.Authenticate();
                        else
                            DataCaptureFileBasedIntegrationLib.ProgramReadsoft.RefreshAuthenticationCookie(clientConfig);

                        //var documentLoader = new DataCaptureFileBasedIntegrationLib.DocumentDownloader();
                        //documentLoader.DownloadOutputDocumentsReadyForExport(clientConfig);


                        var documentServiceClient = new DocumentServiceClient(clientConfig);
                        var documents = documentServiceClient.GetOutputDocumentsByCurrentCustomer();

                        /*foreach (var xmlFile in Directory.GetFiles(sourcePath, "*.xml"))
                        {*/
                        //var invoice = "";

                        foreach (var documentReference in documents)
                        {
                            if (documentReference.OutputOperation == OutputOperationType.Export)
                            {
                                var rejectedInfo = new OutputResult();
                                try
                                {

                                    var document = documentServiceClient.GetDocument(documentReference.DocumentId);
                                    var documentHeader = document.HeaderFields;
                                    var invoiceFile = SetInvoiceData(documentHeader);

                                    string contentType = string.Empty;
                                    var image = documentServiceClient.GetDocumentOutputImage(documentReference.DocumentId, response => { contentType = response.ContentType; });

                                    //var currentFileName = System.IO.Path.GetFileName(xmlFile);
                                    if (invoiceFile != null)
                                    {
                                        //var dateTest = DateTime.ParseExact(invoiceFile.InvoiceDate, "YYYYmmdd", System.Globalization.CultureInfo.CurrentCulture);
                                        DateTime invDate = DateTime.Today;

                                        if (invoiceFile.InvoiceDate.Length > 1)
                                        {
                                            invDate = DateTime.Parse(invoiceFile.InvoiceDate.Insert(6, "-").Insert(4, "-"));
                                        }
                                        //Due date set to today + 20 because of no date
                                        DateTime invDueDate = DateTime.Today.AddDays(30);

                                        if (invoiceFile.InvoiceDueDate.Length > 1)
                                        {
                                            invDueDate = DateTime.Parse(invoiceFile.InvoiceDueDate.Insert(6, "-").Insert(4, "-"));
                                        }

                                        // var invDueDate = DateTime.Parse(invoiceFile.InvoiceDueDate.Insert(6, "-").Insert(4, "-"));
                                        // string imageFile = invoiceFile.ImageFilePath;
                                        //string pdfFile = invoiceFile.ImageFilePath.Replace(".TIF", ".pdf").Replace(".tif", ".pdf");
                                        Fortnox f = null;
                                        //if (connections.TryGetValue(invoiceFile.CorporateGroupNumber, out f))
                                        // if (true)
                                        if (connections.TryGetValue("1", out f))
                                        {
                                            //do while loop som kör ett varv?
                                            var supplier = f.GetSupplierByPGBG(invoiceFile.SupplierBG);
                                            if (supplier == null)
                                                supplier = f.GetSupplierByPGBG(invoiceFile.SupplierPG);
                                            if (supplier == null)
                                            {
                                                f.LoadSuppliers();
                                            }
                                            if (supplier == null)
                                                supplier = f.GetSupplierByPGBG(invoiceFile.SupplierPG);
                                            if (supplier == null)
                                                supplier = f.GetSupplierByPGBG(invoiceFile.SupplierBG);


                                            if (supplier != null)
                                            {
                                                string id = null;

                                                try
                                                {
                                                    f.SetAsActive();
                                                    if (f.GetSupplierInvoice(invoiceFile.InvoiceNumber, supplier.SupplierNumber))
                                                    {
                                                        SetInvoiceStatus("Dubblett", documentServiceClient, documentReference);
                                                        throw new Exception("Dubblett");

                                                        //Set Documentstatus to unexported here
                                                    }

                                                    if (invoiceFile.InvoiceType == "true")
                                                    {
                                                        invoiceFile.InvoiceTotalVatIncludedAmount = invoiceFile.InvoiceTotalVatIncludedAmount >= 0 ? invoiceFile.InvoiceTotalVatIncludedAmount * -1 : invoiceFile.InvoiceTotalVatIncludedAmount;
                                                        invoiceFile.InvoiceTotalVatExcludedAmount = invoiceFile.InvoiceTotalVatExcludedAmount >= 0 ? invoiceFile.InvoiceTotalVatExcludedAmount * -1 : invoiceFile.InvoiceTotalVatExcludedAmount;
                                                        invoiceFile.InvoiceTotalVatAmount = invoiceFile.InvoiceTotalVatAmount >= 0 ? invoiceFile.InvoiceTotalVatAmount * -1 : invoiceFile.InvoiceTotalVatAmount;
                                                    }
                                                    decimal roundOff = invoiceFile.InvoiceTotalVatIncludedAmount - (invoiceFile.InvoiceTotalVatExcludedAmount + invoiceFile.InvoiceTotalVatAmount);

                                                    id = f.CreateSupplierInvoice(invoiceFile.InvoiceType ==/*Changed from Faktura to False"*/ "false" ? "F" : "C", supplier, invDate,
                                                        invDueDate, invoiceFile.InvoiceTotalVatIncludedAmount, invoiceFile.InvoiceTotalVatAmount,
                                                        invoiceFile.InvoiceTotalVatExcludedAmount, roundOff, "", "", invoiceFile.InvoiceNumber, invoiceFile.InvoiceCurrency,
                                                        invoiceFile.PaymentReferenceNumber, invoiceFile.StoreId);
                                                }
                                                catch (Exception ex)
                                                {
                                                    errorFiles.Add(new KeyValuePair<string, string>("Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, ex.Message));
                                                    //Email users that supplier is missing, not included in initial offer!!! Talk to lars-erik. 1 day!
                                                    if (Settings.Default.SendEmailError) emailClient.SendErrorMessage(emailSender, emailErrorReceiver, "Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, ex.Message);
                                                    SetInvoiceStatus("Error", documentServiceClient, documentReference);
                                                    continue;
                                                }
                                                string fileName = id + ".pdf";
                                                try
                                                {
                                                    string pdfFile = DownloadImage(documentServiceClient, documentReference);
                                                    f.UploadFile(pdfFile, id);

                                                    File.Delete(pdfFile);
                                                    SetInvoiceStatus("Success", documentServiceClient, documentReference);

                                                    //Set Documentstatus to exported here
                                                }
                                                catch (Exception ex)
                                                {
                                                    rejectedInfo = new OutputResult { Status = OutputStatus.RejectDocument, Message = "Kunde inte ladda upp faktura" };
                                                    SetInvoiceStatus("Error", documentServiceClient, documentReference);
                                                    //Set Documentstatus to unexported here
                                                }
                                            }
                                            else
                                            {
                                                //Set Documentstatus to unexported here
                                                errorFiles.Add(new KeyValuePair<string, string>("Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, "Leverantör saknas. BG: " + invoiceFile.SupplierBG + " PG: " + invoiceFile.SupplierPG));
                                                if (Settings.Default.SendEmailError) emailClient.SendErrorMessage(emailSender, emailErrorReceiver, "Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, "Leverantör saknas. BG: " + invoiceFile.SupplierBG + " PG: " + invoiceFile.SupplierPG);
                                                SetInvoiceStatus("NoSupplier", documentServiceClient, documentReference);
                                            }
                                        }
                                        else
                                        {
                                            //Set Documentstatus to unexported here
                                            errorFiles.Add(new KeyValuePair<string, string>("Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, "Kunde inte hitta koppling till fortnox för bolag: " + invoiceFile.CorporateGroupNumber));
                                            if (Settings.Default.SendEmailError) emailClient.SendErrorMessage(emailSender, emailErrorReceiver, "Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, "Kunde inte hitta koppling till fortnox för bolag: " + invoiceFile.CorporateGroupNumber);
                                            SetInvoiceStatus("Error", documentServiceClient, documentReference);
                                        }
                                    }
                                    else
                                    {
                                        errorFiles.Add(new KeyValuePair<string, string>("Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, " Kunde inte läsa filen"));
                                        if (Settings.Default.SendEmailError) emailClient.SendErrorMessage(emailSender, emailErrorReceiver, "Fel vid inläsning av leverantörsfaktura med fakturanr: " + invoiceFile.InvoiceNumber, " Kunde inte läsa filen ");
                                        SetInvoiceStatus("Error", documentServiceClient, documentReference);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    // Error occured during download
                                    //
                                    // In a production environment, you might want to decide if the error was due to communication with target system (in this case, a folder on disk)
                                    //  or if the document was activily rejected due to e.g. incorrect data.
                                    //
                                    // In case of communication error, you could just try again by not doing anything, thus allowing this program to download the document again next run.
                                    //  That would require some local logging though to make sure such info could be picked up by an administrator.
                                    //
                                    // In case of rejection, you could send that info to ReadSoft Online. The document will then appear in Verify again with the error message you provide.

                                    string errorMessage = string.Format("Error processing document: {0}", ex.Message);
                                    //Logger.Log(errorMessage);

                                    rejectedInfo = new OutputResult { Status = OutputStatus.RejectDocument, Message = errorMessage };
                                    documentServiceClient.DocumentStatus(documentReference.DocumentId, rejectedInfo);

                                    continue; // <-- Optional. Depends on if you want to try with next document or not in case of errors.
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText(Settings.Default.LogFilePath, "The error is from the exception with a continue: " + ex.Message + Environment.NewLine);
                        continue;
                    }
                    if(errorFiles.Count > 0)
                    {
                        foreach (var errorfile in errorFiles)
                        {
                            File.AppendAllText(Settings.Default.LogFilePath, errorfile.Value + Environment.NewLine);
                            errorFiles.Remove(errorfile);
                        }
                        errorFiles.Clear();
                    }
                   
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(Settings.Default.LogFilePath, "The error is from the exception: " + ex.Message  +  Environment.NewLine );
                throw;
            }
        }

        private static void SetInvoiceStatus(string v, DocumentServiceClient documentServiceClient, DocumentReference documentReference)
        {
            if (v == "Dubblett")
            {
                var rejectedInfo = new OutputResult { Status = OutputStatus.RejectDocument, Message = "Kunde inte ladda upp faktura - Dubblett" };
                documentServiceClient.DocumentStatus(documentReference.DocumentId, rejectedInfo);
            }

            if (v == "NoSupplier")
            {
                var rejectedInfo = new OutputResult { Status = OutputStatus.RejectDocument, Message = "Kunde inte ladda upp faktura - Leverantören verkar inte existera i Fortnox. Kontrollera BG och PG." };
                documentServiceClient.DocumentStatus(documentReference.DocumentId, rejectedInfo);
            }

            if (v == "Error")
            {
                var rejectedInfo = new OutputResult { Status = OutputStatus.RejectDocument, Message = "Kunde inte ladda upp faktura - Fortsätter felet, kontakta Swedwise" };
                documentServiceClient.DocumentStatus(documentReference.DocumentId, rejectedInfo);
            }

            if (v == "Success")
            {
                documentServiceClient.DocumentStatus(documentReference.DocumentId, new OutputResult { Status = OutputStatus.Success });
            }
        }

        private static string DownloadImage(DocumentServiceClient documentServiceClient, DocumentReference documentReference)
        {
            DocumentDownloader.DownloadOutputDocument(documentServiceClient, documentReference, Settings.Default.ImagePath);

            return Settings.Default.ImagePath + documentReference.DocumentId + ".pdf";
        }

        private static void MoveFiles(object xmlFile, string imageFile, object okPath)
        {
            throw new NotImplementedException();
        }

        private static Invoice SetInvoiceData(HeaderFieldCollection documentHeader)
        {
            var documentInvoice = new Invoice()
            {
                InvoiceType = documentHeader.Where(s => s.Type.Equals("creditinvoice")).FirstOrDefault().Text,
                SupplierBG = documentHeader.Where(s => s.Type.Equals("supplieraccountnumber2")).FirstOrDefault().Text,
                SupplierPG = documentHeader.Where(s => s.Type.Equals("supplieraccountnumber1")).FirstOrDefault().Text,
                InvoiceDate = documentHeader.Where(s => s.Type.Equals("invoicedate")).FirstOrDefault().Text,
                InvoiceDueDate = documentHeader.Where(s => s.Type.Equals("invoiceduedate")).FirstOrDefault().Text,
                InvoiceNumber = documentHeader.Where(s => s.Type.Equals("invoicenumber")).FirstOrDefault().Text,
                InvoiceTotalVatExcludedAmount = documentHeader.Where(s => s.Type.Equals("invoicetotalvatexcludedamount")).FirstOrDefault().Text.ToDecimal(),
                InvoiceTotalVatRatePercent = documentHeader.Where(s => s.Type.Equals("invoicetotalvatratepercent")).FirstOrDefault().Text.ToDecimal(),
                InvoiceTotalVatAmount = documentHeader.Where(s => s.Type.Equals("invoicetotalvatamount")).FirstOrDefault().Text.ToDecimal(),
                InvoiceTotalVatIncludedAmount = documentHeader.Where(s => s.Type.Equals("invoicetotalvatincludedamount")).FirstOrDefault().Text.ToDecimal(),
                InvoiceCurrency = documentHeader.Where(s => s.Type.Equals("invoicecurrency")).FirstOrDefault().Text,
                PaymentReferenceNumber = documentHeader.Where(s => s.Type.Equals("paymentreferencenumber")).FirstOrDefault().Text,
                CorporateGroupNumber = documentHeader.Where(s => s.Type.Equals("suppliertaxnumber1")).FirstOrDefault().Text,
                Amountrounding = documentHeader.Where(s => s.Type.Equals("amountrounding")).FirstOrDefault().Text,
                ImageFilePath = "",
            };
            return documentInvoice;
        }
    }

    public class Invoice
    {
        public string InvoiceType { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceDueDate { get; set; }
        public decimal InvoiceTotalVatExcludedAmount { get; set; }
        public decimal InvoiceTotalVatRatePercent { get; set; }
        public decimal InvoiceTotalVatAmount { get; set; }
        public decimal InvoiceTotalVatIncludedAmount { get; set; }
        public string InvoiceCurrency { get; set; }
        public string SupplierPG { get; set; }
        public string SupplierBG { get; set; }
        public string StoreId { get; set; }
        public string PaymentReferenceNumber { get; set; }
        public string CorporateGroupNumber { get; set; }
        public string Amountrounding { get; set; }
        public string ImageFilePath { get; set; }

        /*public string InvoiceType { get; set; }
        public string fakturaNummer { get; set; }
        public string fakturaDatum { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string InvoiceDueDate { get; set; }
        public decimal bruttoBelopp { get; set; }
        public decimal InvoiceTotalVatRatePercent { get; set; }
        public decimal momsBelopp { get; set; }
        public decimal InvoiceTotalVatIncludedAmount { get; set; }
        public string InvoiceCurrency { get; set; }
        public string plusgiro { get; set; }
        public string bankgiro { get; set; }
        public string StoreId { get; set; }
        public string ocrNummer { get; set; }
        public string CorporateGroupNumber { get; set; }
        public string ImageFilePath { get; set; }
        public string amountrounding { get; set; }*/


        public static implicit operator ReadSoft.Services.Client.Entities.Document(Invoice v)
        {
            throw new NotImplementedException();
        }
    }
}
